#test
provider "aws" {
  region = "us-east-1"
}
terraform {
  backend "s3" {
    bucket = "terraformremotebackendapril822"
    key = "terraform.tfstate"
    dynamodb_table = "terraform"
    region = "us-east-1"
  }
}
